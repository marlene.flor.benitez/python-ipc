# /usr/bin/python
# *- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# head [-n 5|10|15] file...
# 10 lines, file o stdin
# --------------------------------------------
import argparse
import sys
# ---------------------------------------------
# ARGUMENTS
parser = argparse.ArgumentParser(description="programa args head")
parser.add_argument("-n","--nlin",type=int,\         
        help="Número de línies",dest="nlin",\         
        metavar="5|10|15", choices=[5,10,15],default=10) 
parser.add_argument("fileList",type=str,\         
        help="fitxer a processar", metavar="file",\         
        nargs="*")
parser.add_argument("-v", "--verbose",action="store_true",default=False)
args = parser.parse_args()
print(args)
# ---------------------------------------------
# HEAD
for elem in args.fitxer:
    fileIn=open(elem[0],"r")

    counter=0
    MAXLIN=args.nlin

    # mostra fins les 10 lineas
    for line in fileIn:
        counter+=1
        print(line, end="")
        if counter == MAXLIN:
          break

    fileIn.close()

exit(0)

