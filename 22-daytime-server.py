# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# El servidor engega i espera a rebre una conexió, quan l'accepta executa un Popen 
# per fer un date del sistema operatiu, retorna la informació i tanca la conexió. 
# També finalitza (és un servidor molt poc treballado
# --------------------------------------------
import sys, socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# reutilitza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# socket listening to HOST on PORT
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# en el moment que es conecta un client accepta la conexio i guarda l'adreça i connexio del client
conn , addr = s.accept()
#print("Conn", type(conn), conn)
print("Connected by: ", addr)
# crear pipe per demanar date
ordre = "date"
pipeData = Popen(ordre, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE)
# Un bucle que llegeix stdout del popen
for line in pipeData.stdout:    
    conn.send(line)
conn.close()

sys.exit(0)
