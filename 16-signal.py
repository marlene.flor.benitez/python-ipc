# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# python3 16-signal nº segons
# --------------------------------------------
import sys, os, signal, argparse
global down
global up
down = 0
up = 0

# Definir argument obligatori
parser = argparse.ArgumentParser(description="Programa de l'alarma") 
parser.add_argument("segons", type=int, help="segons inicials de l'alarma") 
args=parser.parse_args()

# Definir handlers
def usr1 (signum, frame):
    global up
    print(f'Signal d\'incrementar: {signum}')
    seg_actuals = signal.alarm(0)
    signal.alarm(seg_actuals + 60)
    up = up + 1

def usr2 (signum, frame):
    global down
    print(f'Signal de decrementar: {signum}')
    seg_actuals = signal.alarm(0)
    if seg_actuals < 0:    
        print("Queden menys de 60 segons")     
        signal.alarm(seg_actuals)   
    else:     
        signal.alarm(seg_actuals - 60)
    down = down + 1

def hup (signum, frame):
    print(f'Signal de reiniciar: {signum}')
    signal.alarm(args.segons)

def term (signum, frame):
    print(f'Signal de mirar quants segons falten: {signum}')
    seg_falten = signal.alarm(0)   
    signal.alarm(seg_falten)
    print(f'Falten {seg_falten} segons per acabar')

def alarm (signum, frame):
    print(f'Signal de finalitzar: {signum}')
    global up, down
    print(f'Finalitzat. Vegades up: {up}. Vegades down: {down}')
    sys.exit(0)

# Assignar un handler al senyal
signal.signal(signal.SIGHUP, hup) #1
signal.signal(signal.SIGINT, signal.SIG_IGN) #2
signal.signal(signal.SIGUSR1, usr1) #10
signal.signal(signal.SIGUSR2, usr2) #12
signal.signal(signal.SIGALRM, alarm) #14
signal.signal(signal.SIGTERM, term) #15

signal.alarm(args.segons)
print(os.getpid())

while True:
    pass
signal.alarm(0)
sys.exit(0)
