# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Usant el programa d'exemple fork fer que el procés fill (un while infinit) 
# es governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
# "adeu andreu" i finalitza. El programa pare genera el procés fill i finalitza.
# --------------------------------------------
import sys, os, signal

# Programa pare
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

# Bifurcació PARE
pid = os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare", os.getpid(), pid)
    print("Hasta luego Lucas!")
    sys.exit(0)

# Programa FILL
print("Programa fill", os.getpid(), pid)

# Definir handlers
def usr1 (signum, frame):
    print("Hola Radiola!")
def usr2 (signum, frame):
    print("Adeu Andreu!")
    sys.exit(0)

# Assignar handlers
signal.signal(signal.SIGUSR1, usr1) #10
signal.signal(signal.SIGUSR2, usr2) #12

# Bucle infinit
while True:
    pass
print("Hasta luego lucas!")
