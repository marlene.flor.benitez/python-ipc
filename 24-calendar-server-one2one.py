# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# En aquest cas el server fa un bucle ininit, va atenent clients infinitament un darrera 
# l'altre (o si més no s'espera a entendre'ls). És per això que cal governar el servidor, 
# per exemple amb senyals.
# --------------------------------------------
import sys, socket, argparse, signal, os
from subprocess import Popen, PIPE

# arguments
parser = argparse.ArgumentParser(description="server daytime mutiple client")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
args = parser.parse_args()
parser.add_argument("-a", "--any", type=str, dest="any", help="any del calendari", default="2023")
args = parser.parse_args()
print(args)

llistaConnectats = []

# signals
# Definir handlers
def usr1 (signum, frame):
    print(f'senyal: {signum}')
    print(llistaConnectats)
    sys.exit(0)
def usr2 (signum, frame):
    print(f'senyal: {signum}')
    print(len(llistaConnectats))
    sys.exit(0)
def term (signum, frame):
    print(f'senyal: {signum}')
    print(llistaConnectats, len(llistaConnectats))
    sys.exit(0)

# Assignar handlers
signal.signal(signal.SIGUSR1, usr1) #10
signal.signal(signal.SIGUSR2, usr2) #12
signal.signal(signal.SIGTERM, term) #15

# Programa pare
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

# Bifurcació PARE
pid = os.fork()
if pid != 0:
    #os.wait()
    print("Programa pare", os.getpid(), pid)
    print("Hasta luego Lucas!")
    sys.exit(0)

# Programa FILL
print("Programa fill", os.getpid(), pid)

HOST = ''
PORT = args.port
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# reutilitza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# definir per on escolta el server 
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# bucle infinit per escoltar peticions
while True:
    # en el moment que es conecta un client accepta la conexio i guarda l'adreça i connexio del client
    conn, addr = s.accept()
    #print("Conn", type(conn), conn)
    print("Connected by: ", addr)
    llistaConnectats.append(addr)
    # crear pipe per demanar date
    ordre = ["cal", args.any]
    pipeData = Popen(ordre, shell=True, stdout=PIPE)
    # Un bucle que llegeix stdout del popen
    for line in pipeData.stdout:    
        conn.send(line)
    conn.close()

print("acabat")
sys.exit(0)
