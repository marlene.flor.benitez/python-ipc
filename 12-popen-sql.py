# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Es pot fer una primera versió tot hardcoded sense diàleg amb: 
# psql -qtA -F';' training -c “select * from oficinas;”. 
# Executa la consulta “select * from oficinas;” usant psql.
# --------------------------------------------
import argparse, sys
from subprocess import Popen, PIPE

#parser = argparse.ArgumentParser(description="""Exemple popen""")
#parser.add_argument("ruta", type=str, help="directoris a llistar")
#args = parser.parse_args()
command = "PGPASSWORD=passwd psql -qtA -F',' -h g17 -U postgres training -c \"select * from oficinas;\""
pipeData = Popen(command, shell=True, stdout=PIPE)
for line in pipeData.stdout:
   print(line.decode("utf-8"), end="")
exit(0)
