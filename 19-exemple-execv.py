# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Ídem anterior però ara el programa fill execula un “ls -la /”. 
# Executa un nou procés carregat amb execv. Aprofitar per veure les diferents variants
# de exec. Provar cada un dels casos.
# --------------------------------------------
import sys, os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid !=0:
    print("Programa pare", os.getpid(), pid)
    sys.exit(0)
print("Programa fill", os.getpid(), pid)
# segon arg com llista
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-la", "/", "/opt"])
# arguments fixes, no llistes
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-lh","/var/tmp")
# amb p no ruta absoluta, la busca al PATH sense possar-la
#os.execlp("ls", "ls", "-lh", "/opt")
# sense ruta absoluta amb llista com arg2
#os.execvp("uname", ["uname", "-a"])
#os.execv("/bin/bash", ["/bin/bash", "show.sh"])
os.execle("/bin/bash", "/bin/bash", "show.sh", {"nom":"joan", "edat":"25"})

print("Hasta luego lucas!")
sys.exit(0)
