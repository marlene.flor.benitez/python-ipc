# /usr/bin/python
# *- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# head [-n 5|10|15] [-f file]
# 20 lines, file o stdin
# --------------------------------------------
import argparse
import sys
# ---------------------------------------------
# ARGUMENTS
parser = argparse.ArgumentParser(description="programa args head")
parser.add_argument("-n", "--nlin", type=int, choices=[5, 10, 15] ,dest="nlin", help="número de lineas a processar", default=10)
parser.add_argument("-f", "--file", type=str, dest="fitxer",help="fitxer a processar", default="/dev/stdin")
args = parser.parse_args()
# ---------------------------------------------
# HEAD
fileIn=open(args.fitxer,"r")

counter=0
MAXLIN=args.nlin

# mostra fins les 10 lineas
for line in fileIn:
    counter+=1
    print(line, end="")
    if counter == MAXLIN:
        break

fileIn.close()

exit(0)

