# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# python3 14-popen-sql-multi.py -d database -c numclie[...]
# --------------------------------------------
import argparse, sys
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Consulta interactiva""")
parser.add_argument("-d", "--database", type=str, dest="database",help="BBDD a accedir", default="training")
parser.add_argument("-c", "--numclie", type=str, dest="numclie", nargs="*", action="append")
args = parser.parse_args()

command = f'PGPASSWORD=passwd psql -qtA -F\',\' -h localhost -U postgres {args.database}'

pipeData = Popen(command, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE, bufsize=0, universal_newlines=True)

for clie in args.numclie:
    sqlStatement = f'select * from clientes where num_clie = {clie[0]};\n'
    pipeData.stdin.write(sqlStatement + "\n")
    print(pipeData.stdout.readline(), end="")

pipeData.stdin.write(f'\q\n')

exit(0)
