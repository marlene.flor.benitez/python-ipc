# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Exemple bàsic fork amb programa pare que llança programa fill (un while infinit). 
# Observar els PID i la lògica del fluxe de funcionament.
# --------------------------------------------
import sys, os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid !=0:
    os.wait()
    print("Programa pare", os.getpid(), pid)
else:
    print("Programa fill", os.getpid(), pid)
print("Hasta luego lucas!")
sys.exit(0)
