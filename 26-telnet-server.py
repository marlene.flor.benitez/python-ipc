# /usr/bin/python3
#*- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Implementar un servidor i un client telnet. Client i server fan un diàleg.
# Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de cada connexió.
# --------------------------------------------
# python3 26-telnet-server.py [-p port] [-s server]
# --------------------------------------------
import sys, socket, argparse, time, signal, os
from subprocess import Popen, PIPE

# definicio d'arguments
parser = argparse.ArgumentParser(description="server telnet")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
parser.add_argument("-d", "--debug", type=int, dest="debug", choices=[1, 2, 3], default=0, help="modo debug")
args = parser.parse_args()
llistaPeers=[]

# Definició dels handlers
def usr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def usr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def term(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)

# assignar handlers
signal.signal(signal.SIGUSR1,usr1) #10
signal.signal(signal.SIGUSR2,usr2) #12
signal.signal(signal.SIGTERM,term) #15

# Bifurcació PARE
pid = os.fork()
if pid != 0:
    print("Telnet server up: ", os.getpid(), pid)
    sys.exit(0)

# Programa FILL
print(f'Children: {os.getpid()}, {pid}')

# definir variables
HOST = ''
PORT = args.port
SENYAL_ACABAT = bytes(chr(4), 'utf-8')
DEBUG = args.debug

# creem socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# reutilitza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# definir per on escolta el server 
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# bucle per acceptar peticions
while True:     
    conn, addr = s.accept()     
    print(f'debug: Connected by {addr}')
    day = time.strftime("%Y/%m/%d-%H:%M:%S")
    llistaPeers.append(addr)
    # bucle per rebre el que envia el client
    while True:
        rebut = conn.recv(1024)
        # si el client tanca la connexió, surt
        if not rebut or rebut.decode("utf-8")[:-1] == "exit":
            break
        # executem l'ordre
        pipeData = Popen(rebut, stdout=PIPE, stderr=PIPE, shell=True)
        # si mode debug > 0
        if DEBUG > 0:
            print(f'debug: Executed command: {rebut.decode("utf-8")}')
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG > 1:
                sys.stdout.write(str(line,'utf-8'))
        conn.sendall(SENYAL_ACABAT)
    if DEBUG > 2:
        print(f'debug: Connection with server start on {day} until {time.strftime("%Y/%m/%d-%H:%M:%S")}')
    print("Connection done. Waiting for client...")
    conn.close()
s.close()
sys.exit(0)

