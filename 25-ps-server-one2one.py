# /usr/bin/python3
#*- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# El servidor rep l'informe del client i el desa a disc. Cada informe es desa 
# amb el format: ip-port-timestamt.log, on timestamp té el format AADDMM-HHMMSS. 
# Usar un servidor com el de l'exercici anterior, un daemon governat per senyals.
# ---------------------------------------------
# python3 25-ps-server-one2one.py [-p port]
# ---------------------------------------------
import sys, argparse, signal, os, socket, time
from subprocess import Popen, PIPE

# arguments
parser = argparse.ArgumentParser(description="server ps")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
args = parser.parse_args()

# Definició dels handlers
def usr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def usr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def term(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)

# assignar handlers

signal.signal(signal.SIGUSR1,usr1) #10
signal.signal(signal.SIGUSR2,usr2) #12
signal.signal(signal.SIGTERM,term) #15

# Bifurcació PARE
pid = os.fork()
if pid != 0:
    print("Server ps engegat: ", os.getpid(), pid)
    sys.exit(0)

# Programa FILL
print("En background:", os.getpid(), pid)

HOST = ''
PORT = args.port

# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# reutilitza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# definir per on escolta el server 
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# bucle infinit per escoltar peticions
while True:
    # en el moment que es conecta un client accepta la conexio i guarda l'adreça i connexio del client
    conn, addr = s.accept()
    print("Connected by: ", addr)
    #crear nom del fitxer
    filename = f'{addr[0]}-{addr[1]}-{time.strftime("%Y%m%d-%H%M%s")}.log'
    fitxer = open(filename, 'w')
    # bucle infinit per rebre dades fins que el client penja
    while True:
        data = conn.recv(1024)
        if not data: break   
        fitxer.write(str(data))
    conn.close()
    fitxer.close()
