# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# En aquest cas el server fa un bucle ininit, va atenent clients infinitament un darrera 
# l'altre (o si més no s'espera a entendre'ls). És per això que cal governar el servidor, 
# per exemple amb senyals.
# --------------------------------------------
import sys, socket, argparse
from subprocess import Popen, PIPE

# arguments
parser = argparse.ArgumentParser(description="server daytime mutiple client")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
args = parser.parse_args()
print(args)

HOST = ''
PORT = args.port
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# reutilitza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# socket listening to HOST on PORT
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# bucle infinit per escoltar peticions
while True:
    # en el moment que es conecta un client accepta la conexio i guarda l'adreça i connexio del client
    conn , addr = s.accept()
    #print("Conn", type(conn), conn)
    print("Connected by: ", addr)
    # crear pipe per demanar date
    ordre = "date"
    pipeData = Popen(ordre, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE)
    # Un bucle que llegeix stdout del popen
    for line in pipeData.stdout:    
        conn.send(line)
    conn.close()

sys.exit(0)
