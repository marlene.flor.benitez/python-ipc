# /usr/bin/python3
#*- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Els clients es connecten a un servidor, envien un informe consistent 
# en fer "ps ax" i finalitzen la connexió.
# --------------------------------------------
# python3 25-ps-client-one2one.py [-p port] server
# --------------------------------------------
import sys, socket, argparse
from subprocess import Popen, PIPE

# definicio d'arguments
parser = argparse.ArgumentParser(description="server daytime mutiple client")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
parser.add_argument("server", type=str, help="host del server", default="localhost")
args = parser.parse_args()

# establir connexió amb el server
HOST = args.server
PORT = args.port
# creem socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# socket conection stablished to HOST on PORT
s.connect((HOST, PORT))
# obrim pipe per fer el ps ax
ordre = ["ps ax"]
pipeData = Popen(ordre, shell=True, stdout=PIPE)
for line in pipeData.stdout:
    s.send(line)
s.close()

sys.exit(0)
