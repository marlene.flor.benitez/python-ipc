# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
#
# --------------------------------------------
import sys, socket
HOST = ''
PORT = 50001
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# socket conection stablished to HOST on PORT
s.connect((HOST, PORT))
# envia Hello World
s.send(b'Hello world!')
# guardem el que ens contesta el client
data = s.recv(1024)
# tanquem socket
s.close()
# return el que ens ha dit el client
print('Recived', repr(data))
sys.exit(0)
