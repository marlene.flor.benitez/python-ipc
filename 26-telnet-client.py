# /usr/bin/python3
#*- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Implementar un servidor i un client telnet. Client i server fan un diàleg.
# Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de cada connexió.
# --------------------------------------------
# python3 26-telnet-client.py -p port -s server
# --------------------------------------------
import sys, socket, argparse
from subprocess import Popen, PIPE

# definicio d'arguments
parser = argparse.ArgumentParser(description="client telnet")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001, required=True)
parser.add_argument("-s","--server", type=str, help="host del server", default="localhost", required=True)
args = parser.parse_args()

#establir connexió amb el server
HOST = args.server
PORT = args.port
SENYAL_ACABAT = bytes(chr(4), 'utf-8')
# creem socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# socket conection stablished to HOST on PORT
s.connect((HOST, PORT))
# bucle infinit per fer ordres
while True:
    ordre = input('ʕ•́ᴥ•̀ʔっ>>> ')
    s.sendall(bytes(ordre, 'utf-8'))
    # si el cliet vol sortir, surt
    if str(ordre) == "exit":
        break
    # bucle infinit per rebre el que envia el server
    while True:
        data = s.recv(1024)
        # si rep el senyal de "he acabat" del server, deixa d'escoltar
        if data[-1:] == SENYAL_ACABAT:
            print(data[:-1].decode("utf-8")[:-1])
            break
        print(data.decode("utf-8")[:-1])
s.close()
sys.exit(0)
