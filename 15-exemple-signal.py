# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Programa d'exemple del funcionament de signal. 
# Defineix handlers i els assigna als senyals sigalarm i sigterm. 
# Finalitza automàticament en passar els n segons de l'alarma definida.
# ---------------------------------------------------------------------
import sys, os, signal

# Definir handler
def myhandler (signum, frame):
    print("Signal handler with signal:", signum)
    print("Hasta luego Lucas!")
    sys.exit(1)

def myhandler2 (signum, frame):
    print("S'ha rebut el senyal:", signum)
    print("NO HA DE SORTIR")

# Assignar un handler al senyal
signal.signal(signal.SIGALRM, myhandler) # 14
signal.signal(signal.SIGUSR2, myhandler) # 12
signal.signal(signal.SIGUSR1, myhandler2) # 10
# Ignorar funcions predeterminades del senyal 15 i 2
signal.signal(signal.SIGTERM, signal.SIG_IGN) #15
signal.signal(signal.SIGINT, signal.SIG_IGN) #2

# Assignar una alarma d'aqui a 60 segons (desde que s'executa el programa)
signal.alarm(60)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
