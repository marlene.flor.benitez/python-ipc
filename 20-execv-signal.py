# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Ídem anterior però ara el programa fill execula un “ls -la /”. 
# Executa un nou procés carregat amb execv. Aprofitar per veure les diferents variants
# de exec. Provar cada un dels casos.
# --------------------------------------------
import sys, os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid !=0:
    print("Programa pare", os.getpid(), pid)
    sys.exit(0)
print("Programa fill", os.getpid(), pid)
os.execl("/usr/bin/python3", "/usr/bin/python3", "16-signal.py", "70")

print("Hasta luego lucas!")
sys.exit(0)
