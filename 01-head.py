# /usr/bin/python
# *- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# head [file]
#  10 lines, file o stdin
# --------------------------------------------
import sys

fileIn=sys.stdin
if len(sys.argv) == 2:
    fileIn=open(sys.argv[1],"r")

counter=0
MAXLIN=10

for line in fileIn:
    counter+=1
    print(line, end="")
    if counter == MAXLIN:
        break

fileIn.close()

exit(0)
