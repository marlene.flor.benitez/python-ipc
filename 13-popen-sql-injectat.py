# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# Fer que la sentència sql a fer sigui un argument tot entre cometes 
# que es passa com a argument. sql injectat: perills d’injectar codi d’usuari en els programes.
# --------------------------------------------
import argparse, sys
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Consulta interactiva""")
parser.add_argument("sentencia", type=str, help="consulta a processar")
args = parser.parse_args()

command = "PGPASSWORD=passwd psql -qtA -F',' -h localhost -U postgres training"

pipeData = Popen(command, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE, bufsize=0, universal_newlines=True)

pipeData.stdin.write(f'{args.sentencia}\n\q\n')

for line in pipeData.stdout:
   print(line, end="")

exit(0)
