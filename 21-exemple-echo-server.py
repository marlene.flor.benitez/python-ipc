# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# exemple-echo-server.py
# --------------------------------------------
import sys, socket
HOST = ''
PORT = 50001
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# socket listening to HOST on PORT
s.bind((HOST, PORT))
# es queda escoltant
s.listen(1)
# en el moment que es conecta un client accepta la conexio i guarda l'adreça i connexio del client
conn , addr = s.accept()
print("Conn", type(conn), conn)
print("Connected by: ", addr)
# Un bucle infinit que retorna el que el client escriu
while True:
    data = conn.recv(1024)
    # si el client ha trencat la conexió, plega
    if not data: break
    conn.send(data)
conn.close()

sys.exit(0)
