# /usr/bin/python3
#*- coding: utf-8-*
# -------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
# El client es connecta al servidor i aquest li retorna la data i tanca la conexió. 
# El client mostra l adata rebuda i en veure que s'ha tancat la connexió també finalitza.
# --------------------------------------------
import sys, socket, argparse

# arguments
parser = argparse.ArgumentParser(description="server daytime mutiple client")
parser.add_argument("-p", "--port", type=int, dest="port", help="port per obrir connexió", default=50001)
parser.add_argument("-s", "--server", type=str, dest="server", help="host del server", default="localhost")
args = parser.parse_args()
print(args)

HOST = args.server
#PORT = 13
PORT = args.port
# crear socket s
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# socket conection stablished to HOST on PORT
s.connect((HOST, PORT))
# guardem el que ens contesta el servidor
while True:   
    data = s.recv(1024)   
    if not data: break   
    print(repr(data))
# tanquem socket
s.close()
sys.exit(0)
