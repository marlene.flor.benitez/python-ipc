# /usr/bin/python
# *- coding: utf-8-*
# --------------------------------------------
# @MarleneFlor
# Administració de sistemes - UF2 - 2022-23
# --------------------------------------------
#
# 
# --------------------------------------------
import argparse
parser= argparse.ArgumentParser(\
        description="programa exemple arguments", \
        prog="02-arguments.py", \
        epilog="hasta luego Lucas!")
parser.add_argument("-e", "--edat", type=int, dest="useredat", help="edat a procesar", metavar="edat")
parser.add_argument("-n", "--nom", type=str, help="nom del usuari" )
#parser.parse_args()
args=parser.parse_args()
print(args)
print(args.useredat, args.nom)
